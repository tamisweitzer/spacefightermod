
#include <iostream>
#include <string>
#include "Level02.h"
#include "BossBioEnemyShip.h"

using namespace std;

void Level02::LoadContent(ResourceManager *pResourceManager)
{
	cout << "Level2";

	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BossBioEnemyShip.png");

	const int COUNT = 1;

	double xPositions[COUNT] =
	{
		0.5
	};

	double delays[COUNT] =
	{
		0
	};

	float delay = 1.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BossBioEnemyShip *pEnemy = new BossBioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);

}

