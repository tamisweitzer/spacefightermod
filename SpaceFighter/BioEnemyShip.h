
#pragma once

#include "EnemyShip.h"

class BioEnemyShip : public EnemyShip
{

public:

	BioEnemyShip();
	virtual ~BioEnemyShip() { }

	void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);


	// MOD: changed text to add 'Bio'
	virtual std::string ToString() const { return "BioEnemy Ship"; }


private:

	Texture *m_pTexture;

};